# igor.milhit.git

Depuis le début de [cette page](https://igor.milhit.ch), j'ai travaillé dans un dépôt `git`, d'abord privé. Les sources sont forcément publiques, puisqu'il ne s'agit que de HTML, CSS. Mais, je me dis que je peux bien travailler de manière plus ouverte. Depuis la version `v2.0.0`, j'utilise le générateur de site statique HUGO, afin d'y adjoindre un blog et, peut-être, des pages statiques.

## Licence

Ce projet est sous licence `CC-BY` : http://creativecommons.org/licenses/by/4.0/

## Theme

Je construit peu à peu mon propre thème pour HUGO. Bien qu'en cours
d'élaboration, le thème est disponible en ligne :
[portfoliGor](https://framagit.org/iGormilhit/portfoliGor)
