---
title: "Catégories"
date: 2019-08-31T18:10:19+02:00
draft: false
---

Voici la liste des catégories utilisées pour classer les billets de ce blog.
Chaque catégorie a sa propre page, sur laquelle elle est décrite. Enfin, elle
devrait l’être.
