---
title: "Publication numérique"
date: 2019-09-01T08:40:04+02:00
draft: false
---

Cette catégorie regroupe les billets qui explorent les méthodes contemporaines
de publication de texte, méthodes qui tentent de tirer profit des outils
numériques.
