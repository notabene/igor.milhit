---
title: "Explorations"
date: 2020-05-17T19:09:50+02:00
draft: false
---

*Explorations*, parce que, comme bien d'autres, je ressens le besoin de voir
ailleurs, de chercher un autre monde et de le faire sur une planète que l'on
croit connaître de fond en comble, mais il se pourrait bien qu'on ne commence à
peine à comprendre qu'on n'y comprend pas l'essentiel : faire partie du vivant.

Tenter de prendre du recul, prendre aux mots les illusions qu'on se fait sur
notre espèce, et réapprendre à voir des possibles.
