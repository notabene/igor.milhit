---
title: "Autre chose"
date: 2020-03-01T20:44:51+01:00
draft: false
categories: ["traces"]
tags: ["introspection", "écriture"]
slug: autre-chose
---

Autre chose. Il serait temps d'écrire autre chose que les billets du lundi,
même si c'est parfois le mardi, voire le mercredi. Autre chose que le rythme
régulier du train, que la nuit du matin trop tôt, la lumière qui annonce
le printemps, ou le retour de l'heure d'été, écrire autre chose, quelque chose,
ce qui ne va pas de soit, alors que l'on sombre dans le silence, cette
incapacité de la coordination minimale, attention j'ouvre la porte, bonjour,
bonsoir, je vais acheter du sel, je ne sais pas trop pourquoi, des décisions se
forment dans ma tête, puisqu'il le faut bien, je planifie, et je m'étonne que…

Justement. Il s'agirait d'écrire autre chose. Malgré le monde qui s'écroule.
C'est un peu facile de vaciller, déjà, alors que tu n'as vu que les signes
précurseurs des tempêtes annoncées, c'était facile d'avoir l'air solide lorsque
tout allait bien, c'est maintenant que ça commence. Bien sûr, les émotions
doivent prendre leur place, mais si on n'y veille pas, elles la prennent toute,
jouent des bis lorsqu'on ne les rappelle plus, tu leur donne un spot, elle te
prennent la lumière. J'ai bien quelques idées, mais j'ai l'impression d'avoir
pris des coups à l'arrière des genoux, des genoux dans les tripes et va savoir
quels lapins sur la nuque, pourtant c'est la vie, les vivants tombent comme des
mouches.
