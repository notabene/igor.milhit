---
title: "Tintin donne un atelier d'écriture"
date: 2019-08-09T17:21:19+02:00
draft: false
categories: ["traces"]
tags: ["rêve", "écriture"]
slug: tintin-donne-un-atelier-d-écriture
---

La date de l'événement importe peu, elle serait forcément impossible, absurde,
irrespectueuse des invariants perceptifs de l'*homo occidentalis* moderne. Bien
sûr, le cadrage imposé par la technique ne permet pas de comprendre que la
table flanquée de deux bancs est en mesure d'accueillir de nombreuses
participantes et participants. On ne sait d'ailleurs pas s'il y a vraiment des
participantes, la logique voudrait que oui, mais l'inconscient du réalisateur,
ou du scénariste affirme son sexisme, son machisme, ses racines patriarcales, à
ma plus grande honte. « Mais là n'est pas le pire ».

La lumière est diurne, solaire. Il est assis tout à la gauche de la table, vu
du point de focalisation. Je dois être en face de lui, mais il regarde à sa
gauche, vers le reste de l'assistance, hors cadre. À son côté se trouve son
fils, mon père. Il est vraiment pareil à lui-même, en chemise et bretelles, ses
cheveux blancs un peu dressés sur la tête, en arrière, le front en partie
dégarni, la prunelle brillante d'un mélange de sérieux et d'espièglerie. Sa
voix est ancrée, comme ses jambes, ses pieds, dont on devine l'assise solide.
Il rappelle que l'exercice d'écriture portait sur l'animal de son choix et il
précise immédiatement qu'il n'était pas nécessaire d'écrire tout un roman.

Un animal. J'étais surpris de le voir là, de l'entendre parler d'écriture, de
voir mes mots sur le papier, les lignes imparfaites, qui se croisent bien avant
l'infini, juste en dehors de la marge, des ratures, des fautes d'orthographes,
auxquelles l'on reconnaît bien mon style.
