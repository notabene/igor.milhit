---
title: "Lundi à deux inconnues"
date: 2020-02-24T06:20:43+01:00
draft: false
categories: ["traces"]
tags: ["lundi", "écriture", "hominidés"]
slug: lundi-à-deux-inconnues
---

Parfois, avant de prendre le train, peut-être la veille, dans le sommeil ou au
lever, j'attrape une image, un élément du réel, ou un élément manquant au réel,
enfin un bout de machin sélectionné pas complètement volontairement dans
le truc, ce qu'on appelle le réel faute de mieux, et il me suffit d'ouvrir mon
cahier numérique (bien plus compliqué, lent et contraignant qu'un vrai cahier,
mais l'accoutumance est forte), taper cinq ou six commandes pour, enfin,
aligner des mots, empiler des propositions, noyer le sens dans les phrases et
pousser sur le web un paragraphe à la fois, toujours moins régulièrement que
prévu. Il arrive que ça ne fonctionne pas, que l'idée envisagée résiste, une
fausse piste aspire la colonne de mots dans un trou noir, sans gravité
pourtant, mais duquel rien ne saurait sortir.

Et d'autres fois, il n'y a rien, pas le moindre bout de fil à tirer, c'est un
peu comme devoir se sortir du marais en se tirant par ses propres cheveux,
et ce sans même être un baron, pas même un moussaillon de la narration, un
geste impossible, on le sent bien. Alors à quoi bon ?

**La question** qui réduit au silence. Ce qui me rappelle l'animal humain,
apparemment acharné à faire disparaître toute trace de… j'allais dire la
nature, mais ça n'a pas de sens, alors toute trace des conditions de son
existence même, cherche une [carte de la pollution
lumineuse](https://www.lightpollutionmap.info/#zoom=5&lat=5670887&lon=911860&layers=B0FFFFFTFFFFFFFF)
et regarde, il ne reste plus que quelques déserts, un bout de forêt pas encore
déforesté, un peu de glace en voie de disparition et des morceaux de chaînes
montagneuses, sinon c'est la ville, quand avons-nous perdu la possibilité de
pratiquer le monde et la vie comme l'aurait fait un *homo sapiens* ?
