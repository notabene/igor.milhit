---
title: "2020, semaine 25"
date: 2020-06-20T21:04:41+02:00
publishdate: 2020-06-21
draft: false
categories: ["traces"]
tags: ["semaine", "couleurs", "inachevé", "émotion", "sédentaire"]
slug: 2020-semaine-25
---

Comme prévu, je n'ai pas grand chose à lister cette semaine, et je n'ai pas
vraiment envie d'écrire ce billet. « Comme prévu, » car selon mon habitude,
lorsque je me lance dans l'idée d'une série, j'ai le sens de l'inachevé assez
tôt dans ma tentative. Je tourne en boucle bien assez souvent, un besoin de
régularité qui me rassure face à l'instabilité intrinsèque du monde, surtout de
ce qui vit, mais en matière de projet à incrément récurrent, non, un essai,
voire deux et… rien. Or, comme tout est instable, je viens de l'écrire,
on ne sait jamais, ça pourrait changer, malgré mes doutes. \
Pourtant, il y a des choses à écrire, à m'écrire. C'est certainement bizarre de
vouloir à le faire sous cette forme, déposée sur un lieu public, même si c'est
une impasse anonyme des quartiers non cartographiés du Web.

Dans les choses faites, il y a l'utilisation d'une palette de couleurs sur le
site, la palette [*Nord*][1]. Pas grand chose à signaler, et si, comme moi, tu
lis de préférence le Web dans un agrégateur de flux, peu t'importe et c'est
tant mieux. Sinon, deux choses principales cette semaine, l'une étant une
vieille connaissance, on pourrait dire une constante cosmologique de mon
univers psychique, l'autre une configuration nouvelle, bien que construite avec
des modules existants.

De la première, pas grand choses à dire -- c'est le thème du billet. Je suis
toujours autant surpris de l'intensité émotionnelle dans laquelle peuvent me
mettre des événements du quotidien, ce phénomène peut me bousiller assez
facilement une journée et je me sens démuni face à ça. Il existe des périodes
où je peux plus facilement rebondir, prendre du recul, et d'autres où le mieux
que je puisse faire, c'est attendre que ça passe. Ce qui constitue déjà un
progrès par rapport à ce que j'étais en mesure de faire il y a dix ou quinze
ans. Bref, rien de nouveau sous le soleil.

La seconde est plus conjoncturelle, enfin peut-être. Vendredi, j'ai réalisé que
je n'étais pas sorti de chez moi, pas même jusqu'à la boîte au lettre au
rez-de-chaussée, depuis le samedi précédent. D'ailleurs, la sortie suivante a
été le samedi également, pour aller au marché. La sortie hebdomadaire. Et ça
m'a passablement démoralisé, constater que sans raison extérieure, je ne sors
pas. Avec le télétravail, ma semaine se résume à me lever tôt, travailler,
manger, me coucher tôt. Par une sorte de boucle de réaction positive, cette
prise de conscience m'a ôté toute envie de sortir, de marcher… Et cette
disposition résonne avec un autre élément, le fait que depuis plus de huit
mois, je cours très peu, j'ai beaucoup de peine à faire de l'exercice, encore
plus régulièrement, j'ai pris du poids, je m'essouffle vite et voilà que je me
retrouve avec un moi auquel je ne m'identifie pas avec beaucoup de joie.
Pourtant, ce moi est bien là, affalé devant son clavier, je n'ai d'autre choix
que de faire avec.

Et pour l'instant, il faut bien avouer que je ne sais pas trop qu'en faire.

Et une dernière pensée qui m'a traversé la boîte à image intérieure, c'est que
je n'écris plus -- mes derniers textes doivent dater de début avril --, j'ai
besoin de sortir pour pouvoir écrire, simplement stimuler mes sens, sans quoi
pas de pensée surprenante. Je me demande si ces billets hebdomadaires, ou du
moins leur tentative, ne sont pas là pour prendre la place vacante, soit par
peur du vide, soit pour éviter de laisser un espace disponible.


[1]: https://www.nordtheme.com/docs/colors-and-palettes "Documentation au sujet
de la palette Nord sur le site officiel."
