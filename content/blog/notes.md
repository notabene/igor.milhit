---
title: "Quelques notes"
date: 2020-09-16T19:26:48+02:00
draft: false
categories: ["traces"]
tags: ["musique", "tristesse", "tragédie"]
slug: quelques-notes
---

Quelques notes. Une fois de la basse. Une fois du piano. Couleurs et mélodies.
Et une tristesse sans nom, sans mot, sans larme ni sanglot, venue du fond des
âges, des temps où la vie n’avait pas de sens et ça n’était pas une question,
pas un problème. Le soleil rasant du matin dans le salon, sur les boucles
noires des cheveux en bataille, la préhistoire d’une existence, avant
l’apprentissage de la lecture, de l’écriture. Ce qui existe est tragique, c’est
ainsi, la recherche de la paix et du bonheur est vaine, tout au plus de fugaces
instants, et rien d’autre.

Faire des enfants doit changer les idées. Tant pis. Tant mieux.

Il y a des images, d’un bout du système solaire à la vallée du rift en
Afrique de l’Est, l’imaginaire de la désolation poussée à son paroxysme, une
prison d’immense, et à l’autre bout, l’origine rêvée des rêves qui marchent,
qui courent à leur perte, emportant tout ce qui est possible de l’être sur leur
passage. Il y a des images. Mais pas de répit, pas de repos.

De la poussière. Sur une dalle en béton. Afin de voir jouer le vent. Des
fissures ouvertes par de nouvelles pousses. Et une voix qui persiste
à vivre, à respirer, à chanter.

Mon corps brisé, sans direction.
