---
title: "La chasse aux magnolias"
date: 2021-04-03T20:28:18+02:00
draft: false
categories: ["traces", "poésie"]
tags: ["chasse", "magnolias", "fleurs", "promenade"]
slug: la-chasse-aux-magnolias
---

La chasse aux magnolias en fleur se pratique avec lenteur, un pas après
l'autre, au hasard des dérives dans les quartiers résidentiels, aux premières
ou dernières heures de la présence solaire, dans la bise de printemps qui mord
les mains hors des poches, sous un ciel bleu où les nuages transhument à grande
vitesse. On n'en revient jamais. On n'en revient jamais bredouille, grâce aux
petits et moyens bourgeois des temps anciens qui savaient planter des arbres à
fleurs dans les jardins de leurs villas. Ici, on vole avec les yeux. Ce n'est
qu'un début. C'est un bon début. On n'en revient jamais. On n'en revient jamais
bredouille, parce qu'il y a toujours une nouvelle feuille d'érable japonais,
bientôt de chêne, des fleurs de cognassiers perçant au milieu des feuilles de
la dernière pluie, des poils au bout des feuilles de charme, des fleurs de
cerisier… car la zone villa est en fleurs. Pancho Villa, reviens ! Enfin, des
magnolias, un jardin sur deux, roses, pastels, blancs, petits, majestueux,
noueux, recouverts de lichens. Et même les magnolias à grandes fleurs qui
annoncent les canicules estivales.

Jamais on n'en revient. Pourtant, nous en sommes bien revenus, bredouilles et
non bredouilles en même temps, des fleurs plein les iris, nous réchauffer avec
des champignons à la crème et au cognac, des rêves de Commune derrière le
front, un demi-sourire aux lèvres.
