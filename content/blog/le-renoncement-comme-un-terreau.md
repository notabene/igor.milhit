---
title: "Le renoncement comme un terreau"
date: 2019-09-30T06:37:33+02:00
draft: false
categories: ["explorations"]
tags: ["climat", "renoncement", "reconstruire", "esthétique"]
slug: le-renoncement-comme-un-terreau
---

Depuis cinquante, quarante, trente, vingt, dix ans, nous n'avons en aucun cas
progressé, avancé, initié un quelconque geste dans la bonne direction, nous
avons simplement poussé plus loin le bouchon, collé la jauge au sommet de la
graduation, dans le rouge, dans le rouge, le sourire aux lèvres, satisfaits de
ne l'être jamais, alors que depuis au moins un siècle, nous savons, la physique
est solide. Peut-être est-ce normal, la révolution copernicienne à faire est
plus existentiellement affreuse que le décentrement de la Terre, le
décentrement du système solaire, de la galaxie, de l'animal humain passé de
créature de dieu à simple membre de la famille des primates, les *grands*
singes, quelle consolation. Jusqu'à la conscience qui a dû laisser sa place
à la bestiole qui nous échappe, nous échappera toujours.

Voilà qu'il était pénible de quitter notre place privilégiée au sein d'un
Univers rassurant, à notre mesure, et de se retrouver pas si différents qu'une
colonie de bactéries sur un grain de poussière, là par hasard, là de manière
temporaire, voué à la destruction, dans n'importe quel coin d'un univers
démesuré, inconcevable, inhospitalier, et semble-t-il mortel. Mais,
au moins, nous pouvions nous gaver, construire des digues contre l'angoisse,
proliférer comme une maladie et dresser des systèmes hyper-industrialisés
devant la face hideuse de la finitude.

En vain, bien sûr, mais à condition de n'être pas trop regardant, de supporter
le mal du siècle, à chacun le sien, on pouvait encore faire semblant de croire
à l'illusion. Et désormais, cette « fête » est terminée, l'avenir sera pire.
Les rêves de notre inconscient collectif, les rêves hérités de génération en
génération étaient mortifères, ils sont morts. Ils ne sont pas les seuls,
bien sûr. Mon ami est mort, mes amis sont morts, mes grands-pères, mes
grand-mères, mes oncles, et tant d'autres, sont morts. C'est la vie, mais il
y a autre chose aussi qui est mort, et nous ne faisons que commencer à le
comprendre, à le découvrir, il faudra en faire le deuil. Un peu comme si toutes
le voies, toutes les issues de secours étaient condamnées, l'appel de Mars en
est le meilleur signe, la folie désespérée du toxicomane qui ne peut se
résoudre à accepter, à abandonner, à renoncer, à laisser tomber. Et faire face
au manque.

Le manque.

Le manque, duquel tout peut naître. Peut-être pas tout, justement, mais qui est
source de possibles. Alors que le refus de ce manque, la rage de vouloir le
combler coûte que coûte ne peut mener qu'à l'épuisement. Et c'est bien ce
manque, l'abandon qui rend possible son acceptation, le renoncement aux
illusions qui nous occupent l'esprit, qu'il s'agit d'écrire, de dessiner, de
chanter, de peindre, de rêver, de rendre désirable. De reconnaître dans les
petits gestes quotidiens que l'on peut observer ici ou là, à côté de soi.
