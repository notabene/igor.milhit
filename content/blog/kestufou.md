---
title: "Kestufou ?"
date: 2021-03-12T07:43:38+01:00
publishdate: 2021-04-01
draft: false
categories: ["explorations", "traces"]
tags: ["salaire à vie", "travail", "faire", "vivre", "rêve"]
slug: kestufou
---

À genoux dans le bac à sable, on dirait que le [salaire à vie][1] était déjà
une réalité, alors *qu'est-ce tu vas faire ?*

C'est une bonne question. Au moins, c'est une tentative d'ouvrir la porte,
histoire de se balader dans le jardin des envies et des rêves, mieux vaut tard
que jamais, enfant je n'ai jamais su quoi répondre à *qu'est-ce que tu* veux
*faire quand tu seras grand*. Pas plus qu'adolescent ou jeune adulte. Dans les
moments les plus concentrés, j'arrivais à lister, en cachette, des trucs que je
n'avais pas envie de faire, et encore, c'était pas sûr. Ou alors, carrément
indicible : je ne veux pas travailler.

Non. C'est pour moi une évidence, on ne peut pas vouloir *ça*. Je veux bien
faire des trucs, mais pour être avec des gens, et encore pas n'importe
lesquels, des gens normaux, sans cravates ni tailleurs, qui n'ont pas envie de
réussir leur vie (une bonne recette pour l'enfer pour soi, ou pour les autres),
mais juste de vivre puisqu'on est là sans l'avoir jamais demandé, alors faisons
des trucs ensemble, voilà[^1]. C'était ça le paradis, alors que je n'avais pas
réfléchi à tout, mais on s'en fout. Et maintenant que j'ai un peu plus réfléchi
à d'autres trucs, ou dit autrement que j'ai lu des choses écrites par des
personnes qui réfléchissent (ou qui ont aussi lu, vraisemblablement), je me dis
que c'est encore plus une bonne idée : faire de trucs, oui, mais ensemble. Et
que l'important, c'est (un peu) moins ce qu'on fait, que la *manière* dont on
le fait, *ensemble*.

Par exemple, sans propriétaire qui décide pour tout le monde. Et, aussi,
surtout, en étant attentif aux mécanismes du pouvoir et de la domination, pour
s'en défendre, s'en extraire, s'en déshabituer, constamment, parce que c'est
comme la poussière, c'est sans fin. C'est ce genre de chose qui me semble
essentielle pour pouvoir faire des trucs *ensemble*.

D'autant qu'il y a plein de choses *à faire*, que ça plaise ou non. Pour
l'instant, c'est toujours sur les mêmes que ça tombe et vraiment, ça ne peut
pas être, ça ne sera jamais juste. Autant qu'on le fasse *ensemble*. Même si ça
ne résout pas tellement le degré de division du travail nécessaire à une
société comme la nôtre. Ce qui porte à réflexion.

L'autre point, quand je me laisse rêver à un monde où on pourrait faire ce
qu'on veut -- sauf, que c'est une question qui n'a pas tellement de sens, c'est
bien l'idéologie de l'individu libre et non faussé qui fait qu'on se la pose
--, c'est de faire différentes choses, pas tous les jours le même boulot quoi.
Par exemple, moi je ferais bien quelques jours en bibliothèque, d'autre à la
ferme, d'autres encore à donner des cours de quelque chose, et des périodes à
ne rien faire, et puis régulièrement, avec les gens du quartier, faire des
trucs pour le quartier.

Voilà, si on pouvait vraiment faire ce qu'on veut, et bien moi, j'aimerais bien
faire comme ça.

[1]: https://fr.wikipedia.org/wiki/Salaire_à_la_qualification_personnelle "Article wikipedia sur le salaire à vie"

[^1]: Sur ce sujet, lire le magnifique CENTRE INTERNATIONAL DE RECHERCHES SUR
L’ANARCHISME, 2019. *Refuser de parvenir : idées et pratiques*. Paris : Nada.
ISBN 979-10-92457-10-0, dont il sera peut-être question dans un prochain
billet.
