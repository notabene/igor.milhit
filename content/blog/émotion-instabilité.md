---
title: "Règne de l'émotion"
date: 2019-08-06T16:35:17+02:00
draft: false
categories: ["traces"]
tags: ["émotion", "instabilité", "couleur"]
slug: regne-emotion
---

C'est une sorte de lapaglissade, tellement l'évidence se fait peau de banane :
un changement d'émotion et tout le contexte se caméléonne, l'élan se coupe
l'herbe sous les tendons d'achille, enfin, l'émotion pèse, leste… lorsqu'elle
ne se lève pas, vent debout, vent arrière et plane entre les cols enneigés dans
l'atmosphère cassante, le givre polit les bronches, sans renacler jamais,
pourtant trébuche à la vue du vautour moqueur, qui brise la phrase comme les os
de l'éterle, et se réserve ce qui reste de la mœlle.

Et pouquoi cela devrait être une fatalité ? Et pourquoi devrais-je en être
épargné ?
