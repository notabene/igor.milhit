---
title: Croisades
date: 2021-09-11T21:52:42+02:00
categories: ["traces"]
tags: ["neocons", "droitisation"]
draft: false
slug: croisades
---


Je me souviens surtout du lendemain. Ou dans les jours qui suivirent, la
chronologie n’est pas si importante. On croît que tout bascule à une date
précise, alors que ce n’est qu’un effet de prise de conscience. Une décennie
après la chute de l’empire soviétique, on se disait qu’on allait pouvoir se
débarrasser de l’empire capitaliste. Quoique l’on ne le croyait pas vraiment,
on avait surtout vu une guerre du Golfe de plus. Mais quand même, on rêvait de
possibles dérives vers la gauche, pour changer.

Et soudain, les croisades. Non pas par un obscur allumé rejouant ses humanités
dans un grenier d’une maison de maître. Par *{{< abbr text="POTUS"
title="President Of The United States" >}}*. OK, un allumé, mais censé
être un minimum sérieux. De la propagande impérialiste, d’accord, je veux bien,
mais là, croisade et vengeance ? Du haut de mes 27 ans, je suis tombé, cheville
foulée, coccyx fissuré. Sur le cul, quoi.

On en était là ?

Vraiment ?

Je croyais pourtant que les sciences nous avaient suffisamment décentré·es pour
que l’on ne retombe plus dans des pièges aussi grossiers. Alors qu’au
contraire, la plus grande démocratie du monde, montrant le chemin, comme
toujours, élisait de si grossiers personnages. Que tout le monde, ou presque,
allait s’empresser de suivre et d’acclamer. Une de démonstration qu’en effet,
sapiens est plus proche des chimpanzés que des gorilles. Mais vraiment proche.

Comme je n’avais rien de plus important à faire, je suis allé fouiller les
bibliothèques de la Ville et du Canton, universitaires et publiques, me disant
que depuis que j’avais traîné dans la machine à documentaire[^1], les
connaissances sur l’animal humain en général, la biologie, devaient avoir
progressé, suffisamment pour que l’on se moque d’un tel discours obscurantiste.
Les croisades, voyons.

Et de fait, les ouvrages synthétiques sur l’histoire des hominidés, de
l’origine matérielle de la vie et même sur les ontologies matérialistes
montraient bien de substantiels progrès. Mieux que ça, des évolutions, des
changements complets de regards, la réinsertion de l’humain dans son animalité,
au sens noble du terme. Bref, les croisades et les guerres de vengeances (vite
déguisées de milles et une manières peu subtiles), non seulement c’était
ringard, mais à ranger aux oubliettes d’Alzheimer.

En vingt ans, ce grand fossé entre ce que le meilleur de nos cultures est
capable de produire comme savoir sur le monde et ses habitants et les discours
et motivations politiques des gouvernements et des nations, et des
regroupements de nations, s’est fait mer, océan, espace intersidéral, c’est
sidérant.

Heureusement qu’il y a eu le grand badaboum de la finance en 2007. Enfin,
« heureusement », on se comprend, parce que ça encore été une sacrée boucherie
cette affaire. L’une après l’autre, depuis toujours.

Et aujourd’hui, monsieur et madame tout le monde, enfin je veux parler des
beaufs avec de l’épargne, se réveillent et partent en …croisade, justement,
encore cette horreur, contre des moulins à vents. Et sans panache aucun.
Presque la bave aux lèvres.

Est-ce que tout ça méritait un billet de blog ? Certainement pas, mais voilà,
je n’ai pas pu m’empêcher ce soir. Mes plates excuses.

[^1]: Vu ainsi, l'école pouvait me sembler supportable.
