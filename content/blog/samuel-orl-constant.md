---
title: "Rue Samuel-Constant numéro 1"
date: 2021-04-25T20:50:52+02:00
publishdate: 2021-05-02
draft: false
categories: ["traces"]
tags: ["Phil", "ORL", "mort", "vie"]
slug: rue-samuel-constant-1
description: "Visite du lieu où avait habité un ami de l'auteur"
postimage: "clematis-montana.jpg"
postimagedescription: "Fleur de clématite montana rose"
---

L’autre jour[^1], je rentrais en vélo d’une visite chez un
{{< abbr title="Otorhinolaryngologue" text="ORL" >}}
[rive-droite][rd], et après avoir vu une clématite aux fleurs roses pâles, je
me suis retrouvé devant l’école d’ingénieur, sur cette piste cyclable qui
t’engage dans une direction pas tellement logique, mais c’est normal, on ne
fait pas toujours ce qu’on veut -- à vrai dire, c’est plutôt rare, autant ne
pas s’agripper avec raideur à ce qu’on croit vouloir -- et cette fois, je l’ai
pris comme une invitation au détour, une invitation à passer dans ta rue, rue
Samuel-Constant, pour la descendre en direction du numéro 1, là où tu as vécu
les 25 dernières années de ta vie, là où tu es mort, là où j’étais arrivé trop
tard, bien trop tard, un premier juin au soir[^2].

Jusqu’ici, je craignais repasser dans cette rue, ce coin aimé de la ville, aimé
parce que tu y habitais, parce qu’avec toi j’y avais vécu plein de choses, des
drôles, des anodines, des tristes, des glauques parfois. Un coin de rue ami. Va
savoir, ça va faire deux ans que tu n’habites plus là, ni nulle part, que tu
n’habites plus le monde ailleurs que dans nos souvenirs. Et puis, la visite
chez l’ORL avait été un bon moment, un moment réconfortant, un de ces rares
moment où tu as l’impression que la médecine est aussi une science, avec des
mesures objectives, sur lesquelles on peut se mettre d’accord, un de ces
moments comme il doit y en avoir peu dans une vie, où tu as l’impression
d’enfin pouvoir désigner, peut-être même agir. Bref, je n’ai pas été triste, ou
alors pas triste comme prévu, une tristesse beaucoup plus lumineuse,
réconfortante.

Les stores de ton ancien appartement, au rez-de-chaussée, étaient levés.
Manifestement, ça avait été refait. Remarque après 25 ans, ça aurait pu être
refait de ton vivant. C’était clair. Coloré. Il y avait un bouquet de fleurs.
Et une femme assise à une table, certainement devant un ordinateur peut-être
portable, dans un coin vraiment petit, juste là où il y avait ton portrait en
samouraï, chignon et barbichette. C’est bête, bien sûr, les gens qui vivent
sont souvent bêtes quand ils pensent à la place des gens morts, mais je me suis
dit que ça t’aurait plu : place aux jeunes, en quelque sorte. Tu aurais
peut-être préféré vivre, mais tu aimais bien savoir que d’autres vivaient. Quoi
qu’il en soit, moi, ça m’a plu. Il se passe des choses au Samuel-Constant
numéro 1, et en apparence ça a l’air, je ne sais pas comment le dire
autrement, vivant.

Je trouve que c’est un bel hommage.


[^1]: Le mercredi 21 avril 2021, vers les 10:00.
[^2]: Voir [le premier billet de ce blog][phil].

[rd]: https://osm.org/go/0CFnyUNt-- "Approximation de la rive-droite de Genève sur OpenStreetMap"
[phil]: /phil
