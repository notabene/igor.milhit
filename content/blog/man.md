---
title: "Manifeste pour les grands singes et la nature"
date: 2020-08-24T16:34:36+02:00
draft: false
categories: ["explorations"]
tags: ["grands singes", "manifeste", "nature", "déforestation", "primates"]
---

Je reproduis ci-dessous le *Manifeste pour les grands singes et la nature*,
abrégé subtilement en <sup>m</sup>A<sup>n</sup>[^1], tiré de :

BOESCH, Christophe, GRUNDMANN, Emmanuelle et MULHAUSER, Blaise, 2011.
*Manifeste pour les grands singes*. Lausanne : Presses polytechniques et
universitaires romandes. Le savoir suisse, 76. ISBN 978-2-88074-914-9.

----

<sup>m</sup>A<sup>n</sup>

{{< smallcaps "Manifeste pour les grands singes et la nature" >}}

« Les forêts tropicales disparaissent à un rythme effréné et avec elles les
dernières populations de grands singes. Tous les spécialistes sont unanimes :
si nous n’entreprenons rien, gorilles, chimpanzés et bonobos auront disparu
d’ici le milieu du 21<sup>e</sup> siècle. Pour les orangs-outans la situation
est encore plus dramatique ; dans vingt ans ceux-ci pourraient bien ne plus
vivre que dans des zoos.

Il est aujourd’hui urgent de se mobiliser pour stopper cet écocide ! Sauver les
grands singes, c’est sauver les forêts tropicales, un écosystème essentiel pour
la planète. La disparition à grande échelle de ces forêts, résultant d’une
exploitation effrénée et sans aucune limite, met en péril non seulement la
survie de cet écosystème et de sa biodiversité associée, mais aussi celle des
peuples indigènes en dépendant et pose de graves problèmes environnementaux. La
déforestation est aujourd’hui une cause majeure d’émission de gaz à effet de
serre et donc du réchauffement climatique. La disparition de la forêt tropicale
sera immanquablement le prélude à celle d’*Homo sapiens sapiens*, l’Homme
moderne. Le temps est venu de réagir et d’agir… avant qu’il ne soit trop tard !

Nous, citoyens de la Terre, demandons à nos gouvernements et aux instances
internationales d’accepter comme devoir suprême de sauvegarder et protéger les
primates et de tout mettre en œuvre pour :

1. Exiger une gestion durable et respectueuse de l’environnement des forêts
   tropicales, habitats des grands singes.
2. Interdire toute importation de bois tropicaux non reconnus comme provenant
   d’un commerce respectueux de l’environnement répondant aux critères établi par
   la certification FSC.
3. Contribuer à la mise en place d’exploitations de ressources minières (or,
   pétrole, diamant, coltan, fer…) respectueuses de l’environnement et des
   populations locales.
4. Faire cesser le braconnage de grands singes ainsi que le trafic de « viande
   de brousse » associé et celui de jeunes individus vendus comme « animaux de
   compagnie ».
5. Réaliser des contrôles sévères afin de respecter les points 2, 3 et 4 de ce
   manifeste auprès des entreprises travaillant en zone tropicale, notamment
   celles dont le siège social est établi dans nos pays occidentaux respectifs.
6. Engager des moyens financiers importants pour la mise en application des
   clauses 1. à 5., en développant notamment des projets de gestion durable avec
   les populations locales. »

Neuchâtel (Suisse), le 4 avril 2008

[^1]: *manifest for Apes and nature*.
