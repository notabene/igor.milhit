---
title: "Lundi, décompte"
date: 2020-01-06T06:19:19+01:00
draft: false
categories: ["traces"]
tags: ["lundi", "mort", "quotidien"]
slug: lundi-décompte
---

Assise à table, verre de blanc à portée de main, face au village, face au
hameau des origines, face à La Montagne Sacrée, face à la lumière. Face au
destin, dont le degré d'incertitude s'est réduit. Elle affronte la vie qui
grince des dents, courageuse, un peu rageuse peut-être, et envoie tout rigoler,
prépare des peaux de bananes pour La Mort, qui fait moins la fière et assure
les jointures de ses os. On ne sait jamais, c'est quand même un puzzle
compliqué un squelette, surtout lorsqu'il doit s'assembler tout seul.

On rigole, on rigole, c'est facile de penser à La Fin lorsqu'elle se situe
dans une portion floue de l'avenir, un peu moins lorsqu'un proche, qu'une
proche ne retourne plus à la ligne, mais depuis le chapitre suivant on malaxe
son deuil. Là, elle est annoncée, sur la voie 4, toujours dans la même
direction, l'horaire n'est pas clair, mais c'est pour bientôt. Et il faut se
lever le matin, faire la vaisselle, remplir des papiers, classer ses émotions,
peut-être soutenir son entourage, soutenir ses soutiens, bref, vivre sans
attendre, parce qu'on sait bien à quoi s'attendre.

En théorie, ce n'est pas si différent, au jour le jour. En théorie. Parce qu'en
pratique, on pratique la diversion, on botte en touche, on gratte le papier, on
taquine le clavier *qwertz*, surtout on travaille, la sainte drogue réservée
aux prédestiné·es du Marché de l'Emploi, ce dieu sans pitié ni justice.

En pratique, c'est surtout le fardeau qu'on se met soi-même sur le dos, oh, on
a bien appris à le faire, mais on pourrait désapprendre un peu, toute cette
culpabilité poisseuse, qui n'apporte rien d'autre que sa poisse.

Là-bas, dans le coude de la vallée du Rhône, bientôt, se lève le soleil.
