---
title: "Lundi, ce mercredi de plus"
date: 2019-12-18T06:14:14+01:00
draft: false
categories: ["traces", "explorations"]
tags: ["lundi", "imaginaire", "besoin", "exploration"]
slug: lundi-ce-mercredi-de-plus
---

Les semaines se suivent et la régularité s’égare volontairement entre l’ordre
et le chaos, au gré des cahots familiers du train, ma voiture de luxe, mon
transport individuel collectif, toujours si peu en grève, pourtant ça nous
ferait du bien d’ouvrir grand l’avenir et de se risquer à vivre mieux, nous
délester de ce confort mortifère, mesquin, nihiliste. D’autant que ce qui se
prépare, pas trop lentement, depuis deux siècles et qui s’est accéléré ces cinq
dernières décennies, a bel et bien commencé à nous tomber dessus. Et c’est le
réflexe du repli national, les banquiers et les oligarques d’abord, les rats
poussent femmes et enfants à la mer tempétueuse…

Quitte à tout perdre, nous pourrions au moins y gagner de la lumière et de la
chaleur humaine, au moins d’une certaine humanité, peu préoccupée par son
troisième pilier défiscalisé.

Abandonner les chimères, dégonfler les illusions, balayer les mirages et
étendre l’espace des possibles, comment se décoloniser l’imaginaire, sortir ses
rêves et ses désirs du sarcophage capitaliste, mettre à jour sous les couches
poussiéreuses et grasses de la publicité ses besoins, nos besoins, leurs
besoins et leur laisser prendre la place qui leur revient.
