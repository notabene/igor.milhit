---
title: "Ne pas fermer la porte"
date: 2020-05-19T20:30:10+02:00
publishdate: 2020-05-20
draft: false
categories: ["explorations"]
tags: ["possible", "moral", "désir"]
slug: ne-pas-fermer-la-porte
---

*Ne pas fermer la porte*. Ce billet est la suite de *[Ouvrir des
possibles](/ouvrir-des-possibles)*, et c’est amusant parce que je vais aborder
ici ce sur quoi je pensais écrire la dernière fois. Encore que ce n’est pas
tout à fait certain. Parce que je ne sais pas trop comment écrire sans être…,
sans être ce que je suis, un vieux con qui ne sait pas trop comment faire pour
trouver un semblant de sens, afin de continuer à avancer, à accompagner le
mouvement, mais pas n’importe lequel, bref, on sent bien que tout ça n’est pas
très clair. Mais le truc, c’est qu’on ferme trop souvent la porte, je ferme
trop souvent la porte et que c’est devenu complètement irresponsable.

Tu vois, je te l’avais bien dit, le vieux con moralisateur qui sort son gros
doigt grondeur[^1]. *Irresponsable*. Et puis quoi encore ? Chacun, chacune fait
ce qu’elle peut. Mais, quand même, on s’est lancé dans un gros bordel. Et quand
je dis « on », je ne sais pas bien de qui on peut parler, même si tout le monde
n’a pas la même responsabilité.

L’idée est la suivante : quelqu’un partage autour d’elle ou de lui une lecture,
une idée, une façon de faire autrement, quelque chose quoi, avec l’envie que ça
puisse participer à faire réfléchir, à changer de direction, n’importe quoi qui
ne soit pas se résigner à s’envoyer à toute vitesse contre le mur. *Même si,
peut-être bien qu’il n’y a pas d’alternative*.

Franchement, on dirait du Thatcher. "*{{< smallcaps Tina >}}*". "*There is
no such thing as a society*".

Donc, quelqu’un propose un pas dans l’inconnu, un pas dans un espace encore
jamais parcouru, un pas peut-être maladroit, qui n’aurait pas pensé à tout, et
quelqu’un d’autre, par exemple moi, objecte directe, porte fermée, pas
possible, pas bien, pas suffisant. Et c’est facile, parce que l’histoire
humaine connue, qui n’est pas grand chose par rapport à l’histoire humaine si
ce n’est inconnue, du moins mal connue, cette histoire est une longue suite
d’échecs, de solutions catastrophiques, une accumulation de faits
décourageants.

Voilà, on n’a rien gagné. Pas avancé d’un poil. L’horloge égraine les
secondes, la faucheuse rigole sous cap.

Et parfois, c’est légitime, y a un biais à débusquer, une réalité à ne pas
oublier, un ordre de grandeur à garder à l’esprit, un point essentiel escamoté
sous le tapis, on ne peut pas tout laisser passer, c’est paradoxalement ce qui
nous fait avancer, mais l’essentiel est de le faire *en ne fermant pas la
porte*, en ne fermant pas toutes les portes, en laissant la possibilité de
quelque chose, d’une surprise, bonne ou mauvaise. On doit pouvoir se dessiller
les yeux, s’aider sur ce chemin, sans nécessairement s’enfermer. *Même s’il
devait ne pas y avoir de sortie possible*.

Quelque chose me dit que l’enfermement m’obsède. `:thinking:`[^2]

Aussi, il faudrait s’entraîner à débusquer le détail qui est intéressant, qui
donne à penser, qui inspire, qui donne envie, au moins d’essayer. Oui, il
s’agit de faire une révolution copernicienne, mais cette fois pour de vrai, une
inversion des pôles du désir et c’est de l’ordre de l’impossible. Et alors,
pourquoi ne pas le tenter ?


[^1]: HERMANN, 1995. *Sarajevo-Tango*. Dupuis. ISBN 978-2-8001-2269-4. Faudrait
    que je la relise, cette BD, histoire de voir quelles conneries à bien pu
    dessiner Hermann sur ce sujet, bref.
[^2]: En *emoji* dans le texte.
