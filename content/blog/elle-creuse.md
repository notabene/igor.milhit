---
title: "Elle creuse"
date: 2020-01-27T06:16:57+01:00
publishDate: 2020-01-27
draft: false
categories: ["explorations"]
tags: ["lundi", "récit"]
slug: elle-creuse
---

Elle creuse dans les sédiments. Elle creuse un temple dans les couches
géologiques, à la recherche d’une histoire délaissée, une valise oubliée sur
l’aire d’autoroute, un colis piégé dans le sas de sécurité du terminal B, il y
a des millions d’années, sur une autre planète qu’on appelait « maintenant »,
un âge étrange où l’urgence était de tout remplir, afin de masquer l’angoisse,
où l’urgence était de se précipiter vers la fin, site vite, pour ne plus la
voir venir. Elle creuse dans la terre meuble, fend la pierre à coups de barre
à mine, la page à coups de mine de crayon, un dictionnaire d’avant les déluges
à portée de main, elle creuse l’imaginaire et la mémoire, suit les chemins
calligraphiés par les poètes de l’ère moderne, à l’âge de la pierre, elle se
faufile dans les cheminées, traverse les lacs souterrains des bibliothèques du
paléolithique, elle finira bien par trouver le bout du fil, les traces du
scénario, les empreintes du récit qui avait traversé le détroit à marée basse,
en troupeau, entre les carcasses des porte-conteneurs échoués, dans l’épaisseur
des cathédrales tropicales.

Et que fera-t-elle de sa découverte ?
