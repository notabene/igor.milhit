---
title: "Nouvelle Version"
date: 2019-08-01T08:06:22+02:00
draft: false
categories: [ "publication numérique" ]
tags: [ "blog", "hugo", "portfolio" ]
---

Ce billet d'accueil initie une nouvelle configuration de deux choses qui étaient jusqu'ici distinctes : un blog et une page personnelle. Du côté *méthode de publication*, je voulais pouvoir rédiger dans un éditeur de texte (par opposition aux traitements de texte), en utilisant la syntaxe [MarkDown](https://fr.wikipedia.org/wiki/Markdown).

<!--more-->

## Histoire de *blogiGor*

Mes premières tentatives de publier sur le web doivent dater d'un peu plus de dix ans, lorsque j'ai retrouvé une connexion Internet, et il me semble que c'était avec le service web *blogger*. Je pense que j'ai supprimé cet espace il y a déjà bien longtemps et je n'ai aucun souvenir de son URL. Puis, je me suis fait un compte *wordpress* et c'est à partir de là que le nom *blogiGor* existe. Ce blog est encore consultable à l'adresse [blogigor.wordpress.com](https://blogigor.wordpress.com). Il mentionne son successeur, mais en proposant une URL qui ne fonctionne plus (depuis longtemps, je pense).
Il s'agit de *blogiGor* troisième version, lorsque je me suis mis à héberger certains aspects de ma présence sur le web (ou de mon utilisation de services web) dans mon salon, au cours de l'année 2010. Ce blog est consultable à l'adresse [id-libre.org/blogigor](https://id-libre.org/blogigor). J'ai commencé par installer un *wordpress*, puis j'ai migré les contenus sur un [*pluXml*](https://www.pluxml.org/).

Parallèlement, le premier mai 2015, j'ai acquis le nom de domaine *milhit.ch*, afin d'y publier une page personnelle à l'adresse [igor.milhit.ch](https://igor.milhit.ch), en espérant que les requêtes des moteurs de recherche pointent sur celle-ci, afin d'y proposer des données factuelles et relativement maîtrisée. C'est aussi approximativement à cette époque que j'ai commencé à prendre connaissance de l'existence des générateurs modernes de sites statiques. Peu à peu, je trouvais qu'il serait plus logique de réunir mon blog et cette page personnelle. C'est désormais, enfin, le cas.

## Outils

Cela peut sembler étrange, mais je n'ai jamais rédigé de billet en utilisant l'éditeur intégré des différents moteurs de blog que j'ai pratiqué. Avant que je ne m'intéresse vraiment à ce que pouvait vouloir dire *écrire avec des outils numériques*, je tapais mes textes dans un traitement de texte, d'abord en mode machine à écrire, puis de façon plus structurée, et je reproduisais, tant bien que mal, le résultat obtenu dans l'éditeur web. Plus tard, je me suis mis à utiliser la syntaxe MarkDown directement dans un éditeur de texte. Avec cette méthode, je pouvais produire un texte structuré sans utiliser aucun menu et sans être lié à un logiciel ou un format particulier. Ce qui représente pour moi un grand confort.
Une fois le texte rédigé, à l'aide [`pandoc`](https://pandoc.org/), un convertisseur de document universel, on obtient assez facilement une version `html` du texte, prêt à la publication sur le web. Du coup, quand j'ai appris qu'il existait des logiciels offrant la possibilité de rédiger hors connexion (en « local »), avec une syntaxe de balisage légère (comme MarkDown, mais pas uniquement), dans l'éditeur texte de son choix, puis de générer le site web et de le publier, c'est devenu mon graal. D'autant que puisque les contenus du site web en question se résument à une arborescence de fichiers texte, un historique des versions du tout peut aisément être réalisé à l'aide de `git` (ou d'un autre outil du genre).

Je me suis intéressés aux générateurs de sites statiques les plus connus, d'abord [*jekyll*](https://jekyllrb.com/), puis [*pelican*](https://docs.getpelican.com/en/stable/) et enfin [*hugo*](https://gohugo.io/). J'ai passé beaucoup de temps à apprendre *pelican*, j'ai même débuté un thème pour *pelican*, puis à utiliser *hugo* dans un contexte professionnel ([rero21.ch](https://rero21.ch)), avant d'enfin décider de réaliser la fusion de ma page personnelle et de mon blog avec *hugo*.

## De l'esquisse à l'inachevé

Cette transition et cette fusion me prenaient trop de temps, parce que je voulais obtenir quelque chose de suffisamment abouti. Et pendant ce temps, je ne publiais presque plus. J'ai fini par décider que j'allais mettre en ligne quelque chose de lisible, presque fonctionnel, et que j'allais l'améliorer peu à peu, une sorte de démarche « agile » individuelle. Notamment parce que ce projet était devenu le goulet d'étranglement de tous mes autres envies, du point de vue web du moins. Alors voilà, *igor.milhit.ch* n'est pas fini, ça ne s'adapte pas tout à fait bien à vos divers écrans, mais c'est lisible, on peut s'y [abonner](/index.xml) et c'est déjà pas mal.
