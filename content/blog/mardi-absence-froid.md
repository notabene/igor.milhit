---
title: "Mardi, et le froid absent"
date: 2020-01-14T06:12:03+01:00
draft: false
categories: ["traces", "explorations"]
tags: ["lundi", "froid", "creux"]
slug: mardi-froid-absent
---

Au creux des brumes du sommeil, j'avais imaginé l'air du matin figé par le
froid, limpide au toucher, cassant au regard, le béton et le bitume du quai
prisonnier du givre, le rappel du voyage de la Terre dans l'immensité hostile.
Cette poésie là me manque, je la sens tapie, non loin, il ne lui manque pas
grand chose, quelques degrés à peine, une maille qui se défait, en secret, dans
le tissus du système industriel, couverture qui emballe le monde et supporte
quelques broderies, vitrines, services, l'illusion, le mirage, un dogme de
solidité, un credo de sécurité, à peine l'épaisseur d'un papier bible. Comme
une dépendance, une toxicomanie, nous nous sommes engloutis, sans savoir, sans
choisir, dans un tombeau de confort, on étouffe et on ne croit plus savoir
vivre autrement, nous avons fuit des calamités, la mort enfant, la mort jeune,
le froid et la faim, pour découvrir que cet échafaudage s'est construit grâce
à la mort enfant, la mort jeune, le froid et la faim de beaucoup, pour
découvrir que cet échafaudage ne peut durer, et que nous sommes en train de
tomber vers un avenir qui ressemble à un passé aux couleurs passées.

La vie humaine est une calamité et c'est notre seule joie. Vivons-la. Encore
que ce n'est pas là une véritable injonction, plutôt une invite lancée
à moi-même, histoire de me donner le change, de singer l'humain libre, de
marcher dans les traces qui dessinent des chemins contraints devant moi, dans
la page qui n'est blanche qu'à mes yeux défaillants.
