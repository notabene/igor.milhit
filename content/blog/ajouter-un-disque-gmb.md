---
title: "Ajouter un disque dans la collection locale"
date: 2020-12-25T09:43:41+01:00
draft: false
categories: ["musique"]
tags: ["Gmusicbrowser", "ID3", "métadonnées", "collection musicale"]
---

Comment ajouter un disque dans la collection existante ? C'est une question qui
revient régulièrement, alors autant avoir un aide-mémoire et pourquoi pas ici ?
Le processus est assez tordu et pénible, j'en conviens, mais le but recherché
est de tenter d'obtenir un ensemble cohérent (reproduisant les mêmes absurdités
arbitraires) dans lequel il est possible de naviguer et de retrouver ses
billes. Quand il s'agit de quelques milliers d'albums, ça peut être utile.

Le processus utilise deux outils principaux, [*Gmusicbrowser*][gmb] pour la
gestion de la collection et l'écoute de la musique, [*EasyTag*][et] pour la
retouche des métadonnées, les *tags ID3*. Dans ce billet, il ne sera pas du
tout question de la [numérisation d'un vinyl][nv] ou l'importation d'un CD. On
va partir de l'étape suivante : on dispose, d'une façon ou d'une autre, des
fichiers numériques d'un album.

1. Créer les dossiers nécessaires dans le répertoire *Musique* :
    1. Créer un dossier pour l'artiste ou les artistes séparé·es par des
       virgules. S'il s'agit de musique classique orchestrale, on commence par
       les solistes, puis l'orchestre et enfin le ou la cheffe d'orchestre. \
       S'il n'y a pas d'artistes clairement identifiables, on peut directement
       créer le dossier de l'album. \
       Exemple : *Saito Kinen Orchestra, Seiji Ozawa*
    1. Dans le dossier *artiste·s*, créer le dossier de l'album, avec son
       titre. Avec la musique classique, commencer par le nom du compositeur,
       puis le titre de l'œuvre, séparés par un point. \
       Exemple : *Tchaikovsky. Serenade for Strings. Mozart. Kleine Nachtmusik*
1. Ajouter les fichiers des plages musicales dans le dossier de l'album.
1. Ajouter la couverture de l'album au format *JPG* ou *PNG*, nommée
   `cover.jpg` ou `cover.png`.
1. À l'aide d'*EasyTAG*, vérifier et éventuellement corriger les métadonnées :
    1. Dans le cas de la musique classique, reproduire ce qui a été fait pour
       nommer les dossiers dans les champs *artistes*, *artistes de
       l'album* et *album*.
    1. Corriger le champ *genre* qui la plus souvent ne correspond pas à la
       liste bizarre utilisée dans la collection (la consulter en cas de
       doute[^1]).
    1. Vérifier les champs correspondant au numéro de la plage, du disque et du
       nombre total de plages.
    1. Que les informations du label ou de la maison de disque, s'il y en a,
       soient bien dans le champ *copyright*.
    1. Que le lien, s'il y en a un, soit bien dans le champ *URL*.
    1. Que la liste des interprètes et de leurs instruments soient bien dans
       le champ *commentaire* et pas dans le champ *artiste* qui ne contient
       que les noms des artistes. S'il y a des retours à la ligne, les
       supprimer.
    1. Que les compositrices et compositeurs soient dans le champ *compositeur*
       et pas dans le champ *artiste*.
    1. Vérifier que la couverture soit ajoutée au champ *Images*.
1. Grâce au scanner de fichier d'*EasyTAG*, choisir le modèle de nom de fichier
   à utiliser (`%n - %t`, ou `DISC %d - %n - %t (%p)`) et scanner les fichier.
1. Enregistrer les changements.
1. Depuis *Gmusicbrowser* :
    1. Ajouter le nouvel album avec *Bibliothèque / Vérifier l'apparition de
       nouveaux fichiers dans la bibliothèque*.
    1. Chercher le nouvel album et :
        1. Sélectionner l'album complet, et avec un clic-droit *Propriété des
           chansons* ajouter les genres supplémentaires si nécessaires, des
           étiquettes (instruments, pays, styles ou sous-genre, autres
           particularités).
        1. Avec clic-droit *Analyse ReplayGain / Scanner en tant qu'album*,
           ajouter le *ReplayGain*.
1. Bravo, l'album est ajouté ! Simple, non ?

Il semble évident que ce billet va devoir être mis à jour régulièrement, parce
que dans cette liste plein de cas particuliers n'ont pas été signalés et
documentés.

Si tu trouves que ce processus est complètement délirant, tu as raison.
Si tu ne peux pas t'empêcher de réagir, va donc faire un tour en
forêt, si c'est possible. Sinon, je ne sais pas, improvise.


[^1]: Il faudrait sans doute un billet particulier pour expliquer cette liste
et avec encore moins de doute la revoir, ce qui n'est pas prêt d'arriver.


[gmb]: https://github.com/squentin/gmusicbrowser
[et]: https://wiki.gnome.org/Apps/EasyTAG
[nv]: https://id-libre.org/blogigor/article120/numeriser-un-vinyle-avec-audacity "Comment numériser un vinyl avec Audacity"
