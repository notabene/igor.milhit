---
title: "Le 7ᵉ live"
date: 2021-02-26T09:32:18+01:00
draft: false
categories: ["musique"]
tags: ["streaming", "listening", "live"]
slug: le-7e-live
---

*Mise à jour*

Le *7ᵉ live* (de *Radio Relai*) a été publié, avec la liste des morceaux. Voici
le lien pour y accéder directement : [7th
live](https://id-libre.org/live#seventh).

Le lecteur ci-dessous fonctionne également :

{{< published src="https://id-libre.org/audio/7th-live" legend="7th Live" >}}

---

J'annonce ici un nouveau live pour le **samedi 27 février 2021, 20:00
(UTC+1)**. Aux platines, ce sera, ou plutôt seront, vraisemblablement *Radio
Relai*. L'annonce a également été publiée sur [id-libre][annonce].

Le live sera disponible sur sa page habituelle, à savoir
<https://id-libre.org/live>, mais ça fonctionne tout aussi bien avec le
lecteur ci-dessous :

{{< stream legend="Le live du samedi 27 février 2021, dès 20:00" >}}

Ce billet sera mis à jour lorsque l'enregistrement du live sera disponible et
documenté.

[annonce]: https://id-libre.org/live#announce
