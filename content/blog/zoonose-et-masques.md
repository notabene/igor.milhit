---
title: "Zoonose et masques"
date: 2020-08-31T21:03:44+02:00
publishdate: 2020-10-26
draft: false
categories: ["explorations"]
tags: ["grands singes", "déforestation", "zoonose", "masques chirugicaux"]
slug: zoonose-et-masques
---

Voici un nouvel extrait du *Manifeste pour les grands singes et la nature*,
nouvel, parce qu'un [billet précédent][1] reproduit le manifeste lui-même. Dans
ce billet, il s’agit d’une des causes de la disparition des grands singes[^1],
à savoir les maladies. Tout d’abord, le virus Ebola et les chimpanzés :

> « Les chimpanzés du Parc national de Taï ont aussi été atteints par le virus
Ebola. En octobre 1992, huit chimpanzés sont morts en moins de deux semaines
alors qu’ils avaient été vus peu avant en parfaite santé. Deux ans plus tard,
la maladie a fait sa réapparition au même endroit. Douze chimpanzés ont péri en
trois semaines. Une autopsie fut pratiquée sur deux corps. Les échantillons
prélevés sur un jeune révélèrent la cause du décès : une souche jusqu’alors
inconnue du virus Ebola.
>
> Comment donc les chimpanzés ont-ils été contaminés ? Des observations
minutieuses nous ont permis de découvrir que les risques d’être infecté par le
virus augmentaient avec la consommation de viande de Colobe rouge, un singe
frugivore qui est l’une des proies favorites des chimpanzés. L’apparition
soudaine du virus au début des années 1990 avait été corrélée avec un
changement climatique affectant le régime des pluies. En Côte d’Ivoire, les
« années Ebola » ont été mises en rapport avec un déficit des précipitations en
juillet et août. Cette saison sèche avait été suivie de pluies plus importantes
en septembre et en octobre. De telles différences pourraient avoir facilité la
propagation des agents pathogènes d’une espèce à l’autre et entre individus de
toutes espèces confondues -- chauves-souris frugivores rongeurs, primates --
due à une concentration de leurs contacts, sur les mêmes sources de
nourriture. »

Avant ce passage, le lecteur ou la lectrice a pu apprendre que les gorilles
meurent aussi des virus d’Ebola, au point de mettre en danger certaines
populations. Il faut préciser que cette menace s’ajoute à la déforestation et
au braconnage. Ce que je trouve intéressant c’est que d’une part, on se
ressemble tellement entre chimpanzés et humains, que l’on partage des voies de
contamination : en mangeant des autres singes. Mais aussi que l’apparition plus
fréquente de ces maladies est liée à des changements climatiques, pour partie
conséquence de la déforestation.

Suit un paragraphe sur des études qui ont montré que les chimpanzés sont des
réservoirs de maladies tout à fait intéressantes et dangereuses pour l’humain,
forcément. Et enfin, un dernier paragraphe qui a retenu mon attention, je pense
que tu vas comprendre pourquoi :

> « Mais le danger de contamination fonctionne dans les deux sens : si les
humains peuvent attraper des maladies de chimpanzés, de la même façon les
chimpanzés peuvent attraper des maladies d’hommes. Les touristes, les
scientifiques et les braconniers peuvent transmettre des maladies en
s’approchant des grands singes. Nous avons malheureusement observé des cas de
cet ordre à Taï où, par notre simple présence, des virus de la grippe ont été
accidentellement introduits dans la forêt. Par trois fois les chimpanzés les
ont attrapés et, suite à une surinfection, ont développé des pneumonies
mortelles. Afin de protéger les chimpanzés, nous portons désormais à leur
approche des masques chirurgicaux. Les responsables de projets de recherche ou
d’activités d’écotourisme devraient exiger le port d’une telle protection de la
bouche et du nez afin de diminuer drastiquement le risque de transmission de
maladies humaines aux grands singes. »

Et bien oui, les zoonoses des uns sont aussi les zoonoses des autres. Une
preuve sympathique qu’on fait bien partie du vivant. Mais en plus, voilà qu’il
est question de masque. Vu le contexte qui est le nôtre actuellement, l’idée de
mettre un masque en forêt, ici en Côte d’Ivoire, pour observer des chimpanzés,
afin d’éviter de leur transmettre nos maladies, dans un texte qui a déjà dix
ans, permet de prendre un peu de recul.


[^1]: Rappelons que parmi les grands singes, les chimpanzés sont génétiquement
plus proches de nous que des gorilles.

[1]: /manifeste-pour-les-grands-singes-et-la-nature
