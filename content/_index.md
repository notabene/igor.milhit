---
title: "Accueil"
date: 2019-03-14T06:32:39+01:00
draft: false
---

## Personnel

Vieux comme la [crise du pétrole](https://fr.wikipedia.org/wiki/Premier_choc_pétrolier "Article Wikipedia sur le Premier choc pétrolier"), je suis né entre des livres, des taches d'encre et quelques [vinyles](https://www.discogs.com/fr/user/ignami/collection?sort=artist&sort_order=asc "Mon compte Discogs"). Les noyaux des centrales nucléaires se sont [mises à fusionner](https://fr.wikipedia.org/wiki/Fusion_du_c%C5%93ur_d'un_r%C3%A9acteur_nucl%C3%A9aire#Cas_r.C3.A9els "Section Cas réels de l'article Wikipedia sur la Fusion du coœur d'un réacteur nucléaire"), les empires d'après les empires [s'écroulèrent](https://fr.wikipedia.org/wiki/Union_des_r%C3%A9publiques_socialistes_sovi%C3%A9tiques#Derni.C3.A8res_ann.C3.A9es_de_l.27URSS_.281985-1991.29 "Section Dernières années de l'URSS de l'article Wikipedia sur l'URSS") et s'écroulent encore, et à la croissance des montagnes de papiers s'ajoute la croissance des montagnes de déchets électroniques. Au milieu de cette dématérialisation joyeuse, j'observe les [traces éphémères](/blog) de mon cheminement chaotique.

## Professionnel

De métier, je suis *<span itemprop="jobTitle">Spécialiste {{< smallcaps "hes" >}} en information
documentaire</span>*, ce qui peut se traduire par
*artisan en sciences de l’information*. Plus précisément, je produis de
l’information, je veille à son accessibilité et/ou à sa diffusion, à sa
pérennité et à sa sécurité. Bien entendu, cette information peut prendre
différentes formes, depuis les livres jusqu’aux données numériques de
la recherche, en passant par la documentation technique, les documents
administratifs, les articles scientifiques numériques, voire imprimés.
Un domaine, ou plutôt des domaines divers et passionnants.

Dans cet ensemble de tâches, je me suis assez rapidement concentré sur
deux aspects principaux : la compréhension, la gestion et le
développement des systèmes d’informations documentaires, et les
méthodes modernes de publications, à savoir celles qui tentent de
réellement tirer les conséquences du passage au numérique.\
D’une part, j’aime explorer, installer, paramétrer, modifier des
outils comme les {{< smallcaps "sigb" >}}, les {{< smallcaps "cms" >}}, mais aussi comprendre et manipuler les
moteurs de recherche. D’autre part, je m’intéresse de près à la
publication, d’abord sur le Web, au moyen de chaînes éditoriales, de
langages de balisage légers, de convertisseurs universels de formats
comme [`pandoc`](http://pandoc.org/ "site du logiciel Pandoc"), de
générateurs de sites statiques.

Enfin et surtout, comme bien d’autres je suis convaincu que les
sciences de l’information et la philosophie qui entoure
l’[informatique libre](https://fr.wikipedia.org/wiki/Logiciel_libre)
partagent un certain nombre de présupposés et s’enrichissent
mutuellement. C’est pourquoi j’aime échanger et partager
l’information et le savoir faire de la manière la plus ouverte et libre
possible.

Depuis novembre 2016, je suis employé à la centrale
[*<span class="smallcaps">rero</span>*](http://rero.ch "site officiel de RERO"), principalement pour
collaborer à l’écriture des [nouvelles
aventures](https://rero21.ch "blog du projet de transition rero21")
du réseau.
