---
title: Les recettes pour un pain au sarrasin
mainfont: "DejaVu Serif"
---

## Pain au sarrasin

La recette suivante se trouve dans :

TEFFRI-CHAMBELLAND, Thomas et DOBOIN, Nathaniel, 2018. *Une autre idée du
pain : des rizières aux délicieuses recettes naturellement sans gluten*.
Paris : La Martinière. ISBN 978-2-7324-7829-6. P. 94.

### Pour 1 pain de 500 g

Temps de préparation: 15 minutes \
Temps de cuisson: 40 à 50 minutes \
Temps de repos: 1 heure et 15 minutes

Matériel: 1 moule antiadhérent d'1 l  ou 1000 cm³

### Ingrédients

- 300 g de farine de sarrasin.
- 7 g de sel fin.
- 25 cl d'eau à 38 °C.
- 4 g de levure fraîche de boulangerie.
- 20 g de flocons de sarrasin pour la finition.
- Huile pour le moule.

Versez la farine et le sel avec la totalité de l'eau dans la cuve du robot muni
d'une feuille. Mélangez pendant 10 minutes à vitesse lente.

Incorporez la levure fraîche émiettée et continuez à pétrir 2 minutes.

Débarrassez la pâte dans un moule antiadhérent ou très légèrement huilé au
pinceau.

Laissez fermenter 1 heure et 15 minutes à température ambiante à l’écart des
courants d'air, recouvert d'un torchon sans contact avec la pâte.

Préchauffez le four à 240 °C.

Fleurez la surface du pain avec les flocons de sarrasin, puis scarifiez-la
légèrement avec une lame de rasoir.

Enfournez et laissez cuire à 240 °C pendant 20 minutes, puis réglez le
thermostat à 200 °C et prolongez la cuisson de 20 à 30 minutes selon votre
four.

La croûte de votre pain doit être dorée. Si vous disposez d'une sonde, vérifiez
que la température à cœur est supérieure à 96 °C.

Laissez refroidir le pain sur une grille.

## Levain jeune de riz

Cette méthode pour un levain de riz se trouve dans :

TEFFRI-CHAMBELLAND, Thomas, 2019. *Traité de boulangerie au levain*. Paris :
Ducasse éditions. ISBN 978-2-84123-992-4. P. 132

Pour ma part, je l'ai fait avec du sarrasin, pour un pain au sarrasin.

Pour l'obtention d'une souche de levain de riz, le plus simple est d'effectuer
son premier ensemencement à l'aide d'un levain de seigle. Ensuite, tous les
rafraîchis suivants sont bien entendu faits uniquement à partir de farine de
riz.

Après quelques rafraîchis, la proportion de seigle devient négligeable dans le
levain de riz.

Comme pour les levains de blé, les levains de riz doivent être rafraîchis et
gardés à température ambiante avant leur utilisation. Le passage en froid
permet donc uniquement de conserver un chef.

### Ingrédients

- 1000g de farine de riz complet
- 12g de psyllium
- 1000g d'eau à 40 °C
- 1000g de souche de riz

### Méthode de travail

1. Pétrissage : P = 5'
1. Fermentation: 2h à température ambiante avant utilisation

### Précisions

Pour cette recette et les suivantes, pensez toujours à mélanger les poudres
entre elles (farine et psyllium) avant la phase de pétrissage. Pour ce qui
concerne les aspects fermentaires, les temps de chute des farines de riz sont
souvent très hauts et, à conditions de température, d'hydratation et
d'ensemencement égales, les fermentations sont plus lentes sur le riz que sur
le blé ou le seigle.

Pour retrouver des temps de fermentation plus classiques, il s'ensuit donc que
les fabrications à partir de farines de riz sont le plus souvent réalisées à
forte hydratation (proche de 100 %), à température élevée (proche de 30 °C), et
avec de forts ensemencements : 500g à 1000g de levain jeune par kilo de farine.
Ces conditions permettent de travailler ces pâtes fragiles dans des temps plus
courts.

## Pain au sarrasin pur levain

Cette méthode se trouve dans :

TEFFRI-CHAMBELLAND, Thomas, 2019. *Traité de boulangerie au levain*. Paris :
Ducasse éditions. ISBN 978-2-84123-992-4. P. 144

Malgré son appellation courante de « blé noir », le sarrasin n'est pas une
espèce du genre Triticum (genre regroupant les variétés de blé), ni même une
graminée. Il s'agit en effet d'une Polygonacée, plante à grosses fleurs
blanches dont les graines sont naturellement sans gluten. L'absence de gluten
rend le sarrasin difficile à panifier et son usage traditionnel est plutôt
réservé à la fabrication de galettes et de pâtes (les soba japonaises, en
particulier). Originaire de Chine, le sarrasin serait cultivé en Europe depuis
le XIVᵉ siècle.

### Ingrédients

- 1000g de farine de sarrasin
- 4g de psyllium
- 18g de sel
- 900g d'eau à 40°C 
- 1000g de levain jeune de sarrasin
- 2g de levure (facultatif)
- 15g d'huile d'olive 

### Méthode de travail

1. Pétrissage : P = 5'+ 5' + 2' (huile)
1. Façonnage : division et moulage
1. Apprêt: 1h30-3h

**Cuisson au four à sole** : 35min à 230 °C et 20min à 200 °C oura ouvert \
ou \
**Cuisson au four ventilé** : 30min à 240°C et 20min à 200°C oura ouvert \
**Poids du produit** : 400 à 600g pour un moule de 1000 cm³ \
**Température de la pâte** : 30 - 35 °C

### Dégustation

Si certains consommateurs connaissent le goût relativement doux du sarrasin
galette, en soba ou cuit en grains (kasha), la plupart ignorent les arômes
puissants de ce dernier lorsqu'il est fermenté au levain. En milieu acide, le
sarrasin développe des arômes beaucoup plus charpentés, voire âcres. Il n'est
alors pas peu dire que ce pain de sarrasin pur n'est pas un pas consensuel.

### Précisions

Les pâtes de sarrasin pur sont des pâtes fragiles qu'il faudra enfourner à
temps, car elles sont peu tolérantes à l'excès de fermentation. Comme pour les
pains de seigle pur, il est conseillé de les travailler chaudes (à ± 30 °C),
afin d'essayer de préserver au mieux jusqu'à la cuisson les gaz qu'elles ont
piégés car elles sont relativement poreuses. \
Afin de limiter le développement de goûts âcres que le sarrasin développe en
milieu acide, il est conseillé de maintenir des pH relativement hauts, et pour
cela de travailler avec des levains jeunes à forte dose.
